'use strict';

app.service('SeriesService',function(){
	
	var transformar = function(json) { return new Serie(json) }
	 
	this.listSeries = function(){ return this.misSeries.map(transformar);
	}
	
	this.misSeries = [
	      {"nombre" : "Game of Thrones", "temporadas" : "7", "estado" : "Vista"},
	      {"nombre" : "Stranger Things", "temporadas" : "2", "estado" : "Vista"},
	      {"nombre" : "Breaking Bad", "temporadas" : "5", "estado" : "Pendiente"},
	      {"nombre" : "The Fall", "temporadas" : "3", "estado" : "Mirando"},
	      {"nombre" : "The Last Kingdom", "temporadas" : "2", "estado" : "Vista"},
	      {"nombre" : "Touch", "temporadas" : "2", "estado" : "Vista"},
	      {"nombre" : "Shannara Chronicles", "temporadas" : "2", "estado" : "Mirando"},
	      {"nombre" : "Vikings", "temporadas" : "4", "estado" : "Pendiente"},
	      {"nombre" : "Atypical", "temporadas" : "1", "estado" : "Vista"},
	      {"nombre" : "Iron Fist", "temporadas" : "1", "estado" : "Pendiente"},
	      {"nombre" : "The Defenders", "temporadas" : "1", "estado" : "Mirando"},
	      {"nombre" : "The OA", "temporadas" : "1", "estado" : "Vista"},
	    ];		
	
	this.estadoVista = function(_serie){
		this.misSeries.map(serie => serie.nombre == _serie.nombre ? serie.estado = "Vista" : null);
		return this.misSeries.find(serie => serie.nombre == _serie.nombre);
	}
	
	this.estadoMirando = function(_serie){
		this.misSeries.map(serie => serie.nombre == _serie.nombre ? serie.estado = "Mirando" : null);
		return this.misSeries.find(serie => serie.nombre == _serie.nombre);
	}
	
	this.estadoPendiente = function(_serie){
		this.misSeries.map(serie => serie.nombre == _serie.nombre ? serie.estado = "Pendiente" : null);
		return this.misSeries.find(serie => serie.nombre == _serie.nombre);
	}
});
