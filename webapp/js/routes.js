'use strict';

app.config( function($stateProvider, $urlRouterProvider, $locationProvider) {

	$urlRouterProvider.otherwise("/series");
	
	$stateProvider
	.state({
		name: 'series',
		url: '/series',
		templateUrl: 'partials/series.html',
		controller: 'SeguidorDeSeriesController as seriesCtrl',
	})
});


