'use strict';

app.controller('SeguidorDeSeriesController',function(SeriesService){

    this.serieActual = null;

  	this.getSeries = function () {
  		this.series = SeriesService.misSeries;
  	};
    
    this.editarSerie = function(serie) {
        this.serieActual = serie;
    };
    
    
    this.estadoAVista = function(serie) {
    	this.serieEditada = SeriesService.estadoVista(serie);
    	this.updateSerie(this.serieEditada);		
    };
        
    this.estadoAMirando = function(serie) {
        this.serieEditada = SeriesService.estadoMirando(serie);
        this.updateSerie(this.serieEditada);	
    }; 
    
    this.estadoAPendiente = function(serie) {
    	this.serieEditada = SeriesService.estadoPendiente(serie);
    	this.updateSerie(this.serieEditada);
    };
    
    this.updateSerie = function(serie) {
    	this.editarSerie(serie);
    	this.getSeries();
    };
    
    this.getSeries();
});